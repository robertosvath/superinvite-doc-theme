<div class="col-md-4 col-xl-3 order-last order-md-first">
            <hr class="d-md-none">
            
            <!--
            <aside class="sidebar sidebar-sticky sidebar-stick-shadow pr-md-5 br-1">
              <ul class="nav nav-sidebar nav-sidebar-hero" data-accordion="true">
                <li class="nav-item">
                  <a class="nav-link active" href="#">Getting started <i class="nav-angle"></i></a>
                  <div class="nav">
                    <a class="nav-link active" href="kb-introduction.html">Introduction</a>
                    <a class="nav-link" href="kb-contents.html">Contents</a>
                    <a class="nav-link" href="kb-basic-starter.html">Basic starter</a>
                    <a class="nav-link" href="kb-expert-starter.html">Expert starter</a>
                  </div>
                </li>
              </ul>
            </aside>
            -->

            <aside class="sidebar sidebar-sticky sidebar-stick-shadow pr-md-5 br-1">
              <?php 

              class Custom_Sublevel_Walker extends Walker_Nav_Menu {

                  function start_lvl(&$output, $depth=0, $args=array()) {  
                    $output .= '<div class="'.$args->submenu_class.'">';                    
                  }
               
                  function end_lvl(&$output, $depth=0, $args=array()) {
                    $output .= '</div>';                    
                  }
               
                  function start_el(&$output, $item, $depth=0, $args=array(), $current_object_id=0) {
                      $link = '<a href="'.$item->url.'" class="nav-link '. ($item->is_active?'active':'').'">' . esc_attr($item->title) . ($item->has_children?' <i class="nav-angle"></i>':'') . '</a>';

                      if($depth>0) {
                        $output .= $link;
                      } else {
                        $output .= '<li class="nav-item">' . $link;
                      }                      
                  }
               
                  function end_el(&$output, $item, $depth=0, $args=array(), $current_object_id=0) {
                    if($depth>0) {                      
                      $output .= '';
                    } else {
                      $output .= '</li>';                      
                    }
                  }

                  function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
                      
                      $id_field = $this->db_fields['id'];
                      if ( is_object( $args[0] ) ) {
                          $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );

                          if($args[0]->has_children) {
                            $element->url = "#";
                            $element->has_children = true;
                          }
                      }

                      $classes = $element->classes;
                      if( /*$item->menu_item_parent == 0 && */
                        in_array( 'current-menu-item', $classes ) ||
                        in_array( 'current-menu-ancestor', $classes ) ||
                        in_array( 'current-menu-parent', $classes ) ||
                        in_array( 'current_page_parent', $classes ) ||
                        in_array( 'current_page_ancestor', $classes )
                        ) {

                        $element->is_active = true;
                      } else {
                        $element->is_active = false;
                      }

                      return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
                  }                  
              }

              wp_nav_menu( array( 
                'theme_location' => 'side-menu', 
                'menu_class' => 'nav nav-sidebar nav-sidebar-hero',
                'submenu_class' => 'nav',
                'depth' => 0,
                'walker' => new Custom_Sublevel_Walker()
              ) ); 
              
            ?>
            </aside>
          </div>
