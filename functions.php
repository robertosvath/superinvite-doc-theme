<?php

if ( ! function_exists( 'superinvitedoc_setup' ) ) :
/**
* Sets up theme defaults and registers support for various WordPress features
*
*  It is important to set up these functions before the init hook so that none of these
*  features are lost.
*
*  @since SuperinviteDoc 1.0
*/
function superinvitedoc_setup() {

		/**
     * Make theme available for translation.
     * Translations can be placed in the /languages/ directory.
     */
    load_theme_textdomain( 'superinvitedoc', get_template_directory() . '/languages' );
 
    /**
     * Add default posts and comments RSS feed links to <head>.
     */
    add_theme_support( 'automatic-feed-links' );
 
    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails' );
 
    /**
     * Add support for two custom navigation menus.
     */
		register_nav_menus( array(
	    'primary-menu'   => __( 'Main Menu', 'superinvitedoc' ),
	    'side-menu' => __( 'Side Menu', 'superinvitedoc' )
		) );
 
    /**
     * Enable support for the following post formats:
     * aside, gallery, quote, image, and video
     */
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );

}
endif;
add_action( 'after_setup_theme', 'superinvitedoc_setup' );

function add_additional_class_on_li($classes, $item, $args) {
	if($args->add_li_class) {
		$classes[] = $args->add_li_class;
	}
	return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

function add_menu_link_class( $atts, $item, $args ) {
  if (property_exists($args, 'link_class')) {
    $atts['class'] = $args->link_class . ($item->is_active?' active':'');
  }

  return $atts;
}
add_filter( 'nav_menu_link_attributes', 'add_menu_link_class', 11, 3 );

function add_active_class($classes, $item) {

  if( /*$item->menu_item_parent == 0 && */
    in_array( 'current-menu-item', $classes ) ||
    in_array( 'current-menu-ancestor', $classes ) ||
    in_array( 'current-menu-parent', $classes ) ||
    in_array( 'current_page_parent', $classes ) ||
    in_array( 'current_page_ancestor', $classes )
    ) {

    $classes[] = "active";
    $item->is_active = true;
  } else {
    $item->is_active = false;
  }

  return $classes;
}

add_filter('nav_menu_css_class', 'add_active_class', 10, 2 );
