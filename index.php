<?php get_header(); ?>

<div class="col-md-7 col-xl-8 ml-md-auto py-8">
	<?php while ( have_posts() ) : the_post(); ?>
	  <article class="<?php post_class(); ?> col-md-7 col-xl-8 ml-md-auto py-8 order-first order-md-last" id="post-<?php the_ID(); ?>">
	    <h1 class="entry-title"><?php the_title(); ?></h1>

	    <section class="entry-content">
	      <?php the_content(); ?>
	    </section>
	  </article>
	<?php endwhile; ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>