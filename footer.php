</div>
      </div>
    </main><!-- /.main-content -->


    <!-- Footer -->
    <footer class="footer">
      <div class="container">
        <div class="row gap-y align-items-center">

          <div class="col-md-9">
            <div class="nav justify-content-center justify-content-md-start">
              <a class="nav-link" href="<?php echo home_url(); ?>">Home</a>
              <a class="nav-link" href="https://www.superinvite.no">Superinvite</a>
              <a class="nav-link" href="https://www.superinvite.no/#/login">Login</a>

            </div>
          </div>

          <div class="col-md-3 text-center text-md-right">
            <a href="http://thetheme.io">© <?php echo date('Y'); ?> Invite AS</a>
          </div>
        </div>
      </div>
    </footer><!-- /.footer -->


    <!-- Scripts -->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/page.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/script.js"></script>

  </body>
</html>