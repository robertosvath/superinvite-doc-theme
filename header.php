<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <title><?php bloginfo('site_title'); ?></title>

    <!-- Styles -->
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/page.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-touch-icon.png">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png">

  </head>

  <body>


    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark navbar-stick-dark" data-navbar="static">
      <div class="container">

        <div class="navbar-left">
          <button class="navbar-toggler" type="button">&#9776;</button>
          <a class="navbar-brand" href="<?php echo home_url(); ?>">
            <img class="logo-dark" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-padded.png" alt="logo" style="max-height:70px;">
            <img class="logo-light" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-padded.png" alt="logo" style="max-height:70px;">
          </a>
        </div>

        <section class="navbar-mobile">
          <span class="navbar-divider d-mobile-none"></span>

          <ul class="nav nav-navbar ml-auto">
            
            <li class="nav-item">
              <a class="nav-link" href="#">Docs <span class="arrow"></span></a>
              <nav class="nav">
                <a class="nav-link" href="../docs/kb-introduction.html">Getting started</a>
                <a class="nav-link" href="../docs/typography.html">Content</a>
                <a class="nav-link" href="../docs/navbar.html">Layout</a>
                <a class="nav-link" href="../docs/accordion.html">Components</a>
              </nav>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="#">Help Center <span class="arrow"></span></a>
              <nav class="nav">
                <a class="nav-link" href="../docs/kb-introduction.html">Getting started</a>
              </nav>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="#">Login</a>
            </li>

          </ul>
        </section>

      </div>
    </nav><!-- /.navbar -->


    <!-- Main Content -->
    <main class="main-content">
      <div class="container">
        <div class="row">